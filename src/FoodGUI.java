import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

class FoodGUI {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea orderedItemsList;
    private JButton curryButton;
    private JButton takoyakiButton;
    private JButton sushiButton;
    private JButton cancelButton;
    private JLabel totalArea;
    private JTextArea priceArea;
    private JButton checkOutButton;

    private List<String> orders = new ArrayList<>();
    private List<Integer> prices = new ArrayList<>();
    private int totalPrice = 0;

    private void updateOrderList() {
        StringBuilder orderText = new StringBuilder();
        StringBuilder priceText = new StringBuilder();
        for (int i = 0; i < orders.size(); i++) {
            orderText.append(orders.get(i)).append("\n");
            priceText.append(prices.get(i)).append(" 円\n");
        }
        orderedItemsList.setText(orderText.toString());
        priceArea.setText(priceText.toString());
        totalArea.setText("Total Price: " + totalPrice + " 円");
    }

    private void addOrder(String food, int price, String options) {
        orders.add(food + " " + options);
        prices.add(price);
        totalPrice += price;
        updateOrderList();
    }

    private void selectSizeAndAddOrder(String food, int basePrice) {
        String[] sizes = {"Big (+100円)", "Normal", "Small (-100円)"};
        int sizeOption = JOptionPane.showOptionDialog(null, "Select size for " + food,
                "Size Selection", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                null, sizes, sizes[1]);

        if (sizeOption == JOptionPane.CLOSED_OPTION) {
            return;
        }

        int price = basePrice;
        String size = "(Normal)";

        if (sizeOption == 0) {
            price += 100;
            size = "(Big)";
        } else if (sizeOption == 2) {
            price -= 100;
            size = "(Small)";
        }

        if (food.equals("Sushi")) {
            String[] wasabiOptions = {"With Wasabi", "Without Wasabi"};
            int wasabiOption = JOptionPane.showOptionDialog(null, "With or without wasabi?",
                    "Wasabi Selection", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                    null, wasabiOptions, wasabiOptions[0]);

            if (wasabiOption == JOptionPane.CLOSED_OPTION) {
                return;
            }

            String wasabi = wasabiOption == 0 ? "With Wasabi" : "Without Wasabi";
            size += " (" + wasabi + ")";
        }

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " " + size + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == JOptionPane.YES_OPTION) {
            addOrder(food, price, size);
            food = addMisoSoup(food);
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "!");
        }
    }

    private String addMisoSoup(String foodname) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to add Miso Soup with " + foodname + "?",
                "Miso Soup",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == JOptionPane.YES_OPTION) {
            orders.add("   + Miso Soup");
            prices.add(150);
            totalPrice += 150;
            updateOrderList();
            foodname += " with Miso Soup";
        }
        return foodname;
    }

    private ImageIcon resizeImage(String path, int width, int height) {
        try {
            BufferedImage img = ImageIO.read(getClass().getResource(path));
            Image scaledImg = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            return new ImageIcon(scaledImg);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public FoodGUI() {
        tempuraButton.setIcon(resizeImage("/images/tempura.png", 100, 100));
        ramenButton.setIcon(resizeImage("/images/ramen.png", 100, 100));
        udonButton.setIcon(resizeImage("/images/udon.png", 100, 100));
        curryButton.setIcon(resizeImage("/images/curry.png", 100, 100));
        takoyakiButton.setIcon(resizeImage("/images/takoyaki.png", 100, 100));
        sushiButton.setIcon(resizeImage("/images/sushi.png", 100, 100));

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSizeAndAddOrder("Tempura", 300);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSizeAndAddOrder("Ramen", 1000);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSizeAndAddOrder("Udon", 700);
            }
        });
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSizeAndAddOrder("Curry Rice", 500);
            }
        });
        takoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSizeAndAddOrder("Takoyaki", 600);
            }
        });
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSizeAndAddOrder("Sushi", 1200);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel your order?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == JOptionPane.YES_OPTION) {
                    orders.clear();
                    prices.clear();
                    totalPrice = 0;
                    updateOrderList();
                    JOptionPane.showMessageDialog(null, "All orders have been canceled.");
                }
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == JOptionPane.YES_OPTION) {
                    JOptionPane.showMessageDialog(null, "Payment of " + totalPrice + " 円 confirmed.");
                    orders.clear();
                    prices.clear();
                    totalPrice = 0;
                    updateOrderList();
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
